#!/bin/sh

# The script expects one argument: the name of the directory where the file output.sas.
# The file output.sas is the output of the translate component of the Fast Downward planning system.
# Translate the problem description from SAS to TCPP, finds a solution and validates the plan.

mkdir -p $1
picat sas_to_picat $1 < $1/output.sas # Creates the problem description in Picat.
rm -f common_translate_lib.qi # To prevent incorrect behaviour of the Picat program common_translate_lib.pi.
min_makespan=`picat picat_to_tcpp -path $1 $1 | { read min_makespan; echo $min_makespan; }` 
    # Creates the problem description in TCPP and computes the minimum makespan. 
    # Various parameters can be passed to the program picat_to_tcpp.pi (see the documentation).
    # Use picat picat_to_rres -path $1 $1 for to translate to the Relaxed Relaxed Exists-Step encoding instead of TCPP 
    #   (does not compute the minimum makespan). Various parameters can be passed to the program.
    # Use picat picat_to_reinf -path $1 $1 for to translate to the Reinforced Encoding instead of TCPP 
    #   (does not compute the minimum makespan) 
    # or min_makespan=`picat picat_to_reinf -path $1 $1 i | { read min_makespan; echo $min_makespan; }` (with the parameter 'i')
    #   to compute the minimal makespan. Various additional parameters can be passed to the program.

result=0
i=$min_makespan # Use i=1 if minimum makespan was not computed.
if [ "$i" -gt 0 ] ; then
    while [ "$result" -eq 0 ] ; do
        echo "Iteration " $i
        result=`picat tcpp_sat.pi -path $1 $i $1 | { read result; echo $result; }`
            # Attempts to find a plan with the given makespan.
            # Outputs 1 if a plan was found, otherwise outputs 0.
            # Use result=`picat rres_sat.pi -path $1 $i $1 | { read result; echo $result; }` to solve with 
            #   the Relaxed Relaxed Exists-Step encoding instead of TCPP.
            # Use result=`picat reinf_sat.pi -path $1 $i $1 | { read result; echo $result; }`to solve with 
            #   the Reinforced Encoding instead of TCPP.
        i=$(($i+1))
    done
    echo "Solved by" $i "steps."
else
    echo "Could not be solved."
fi
timeout 2m picat validate_plan -path $1 1 < "$1/solution.plan" # Validates plan.
picat validate_plan -path $1 0 < "$1/solution.plan" # Validates sequential plan (only in one action ordering in a parallel step).