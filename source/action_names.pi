module action_names.

import map.

% Name is the name of the action with the identifier Index.
get_action_name(Index) = Name =>
    $op(Index, Name).

% Writes an action to the output file.
write_action(Writer, ActionIndex, T) =>
    Name := get_action_name(ActionIndex),
    writef(Writer, "%d: %s\n", T, Name).

% Writes each action with the corresponding time to the output file.
write_action_names(Writer, Actions) =>
    T := 1,
    foreach (Timestep in Actions)
        AID := 1,
        foreach (A in Timestep)
            if A == 1 then
                write_action(Writer, AID, T)
            end,
            AID := AID + 1
        end,
        T := T + 1
    end.

% Writes action names of actions corresponding to Solution.
% Solution is expected to be a list/array of arrays containing ones and zeros (1 is action is used, 0 otherwise).
% Each element in the list/array Solution represents one timestep, each element in one timestep represents an actions.  
write_names(Solution, OutDir) => 
    FileName := to_fstring("%s/solution.plan", OutDir),
    Writer = open(FileName, write),
    write_action_names(Writer, Solution),
    close(Writer).