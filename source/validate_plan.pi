module validate_plan.

import util.
import problem_def.
import map.

% Creates the array Vars containing on the i-th position the value of the i-th variable and sets initial values.
init = Vars =>
    VarsCount := length(findall(VarIndex, var(VarIndex, _))),
    Vars := new_array(VarsCount),
    AllVars := findall(varval(Var, Val), init(Var, Val)),
    foreach ($varval(Var, Val) in AllVars)
        $var(VarIndex, Var),
        $val(VarIndex, Val, ValIndex),
        Vars[VarIndex] := ValIndex
    end.

% Verifies preconditions of Operator on variables in Vars at the time Time.
verify_preconditions(Vars, Operator, Time) =>
    AllPreconditions := findall(varval(Var, Val), precond(Operator, Var, Val)),
    foreach ($varval(Var, Val) in AllPreconditions)
        $var(VarIndex, Var),
        $val(VarIndex, Val, ValIndex),
        if ValIndex !== Vars[VarIndex] then
            $val(VarIndex, RealVal, Vars[VarIndex]),
            writef(stderr, "Validation failed.\nPrecondition of %s is not satisfied at time %d.\nValue of variable %s: %s (required %s).\n", Operator, Time, Var, RealVal, Val),
            false
        end
    end.

% Applies effects of Operator on variables in Vars.
apply_effects(Vars, Operator) =>
    NewVars := Vars,
    AllEffects := findall(varval(Var, Val), effect(Operator, Var, Val)),
    foreach ($varval(Var, Val) in AllEffects)
        $var(VarIndex, Var),
        $val(VarIndex, Val, ValIndex),
        Vars[VarIndex] := ValIndex
    end.

% Verifies, if all goals are satisfied.
verify_goals(Vars) =>
    AllGoals := findall(varval(Var, Val), goal(Var, Val)),
    foreach ($varval(Var, Val) in AllGoals)
        $var(VarIndex, Var),
        $val(VarIndex, Val, ValIndex),
        if ValIndex !== Vars[VarIndex] then
            $val(VarIndex, RealVal, Vars[VarIndex]),
            writef(stderr, "Validation failed.\nGoal not satisfied.\nValue of variable %s: %s (required %s).\n", Var, RealVal, Val),
            false
        end
    end.

% Verifies all possible sequential plans (all possible permutations of parallel actions in this time step).
% Vars are state variables and the list [HActions | RActions] contains all actions in one time step.
verify_time_step(Vars, [HActions | RActions], Time) =>
    VarsCopy := new_array(length(Vars)),
    foreach (I in 1..length(Vars))
        VarsCopy[I] := Vars[I]
    end,
    if length(HActions) > 0 then
        Permutations := permutations(HActions), 
        foreach (Perm in Permutations)  
            foreach (I in 1..length(Vars))
                Vars[I] := VarsCopy[I]
            end,
            foreach (A in Perm) 
                verify_preconditions(Vars, A, Time),
                apply_effects(Vars, A), 
            end,
            if length(RActions) > 0 then
                verify_time_step(Vars, RActions, Time + 1)
            end
        end
    end.

% Verifies only one plan (only one permutation of actions in each time step).
verify_plan_relaxed =>
    Vars := init(),
    Actions := read_actions(),
    T := 1,
    if length(Actions) > 0 then
        foreach (TimeStep in Actions)
            foreach (A in TimeStep)
                verify_preconditions(Vars, A, T),
                apply_effects(Vars, A)
            end,
            T := T + 1
        end
    end, 
    verify_goals(Vars),
    println("Sequential plan verified.\n").

% Verifies the plan. Checks if all orderings of actions in one parallel step are valid.
% The plan is read from the standard input.
verify_plan =>
    Vars := init(),
    Actions := read_actions(),
    if length(Actions) > 0 then
        verify_time_step(Vars, Actions, 1)
    end, 
    verify_goals(Vars),
    println("Plan verified.\n").

% Reads actions from the standard input.
% RActions is a list of lists. The i-th list contains all actions executed during the i-th time step.
% On each line, there is the time and the name of an action: Time: Action.
read_actions = RActions =>
    Actions := [],
    C := ' ',
    Time := 1,
    TActions := [],
    while (C !== end_of_file)
        C := read_char(),  
        if C !== end_of_file then
            T := 0,
            I := parse_term(to_string(C)),
            while (int(I))
                T := T * 10 + I,
                C := read_char(),
                I := parse_term(to_string(C))
            end,
            _ := read_char(),
            A := read_line(),
            if T == Time then
                TActions := [A | TActions]
            else
                Time := T,
                Actions := [TActions | Actions],
                TActions := [A]
            end
        end
    end,
    if TActions !== [] then
        TActions := reverse(TActions),
        Actions := [TActions | Actions]
    end, 
    RActions := reverse(Actions).

% Verifies the plan.
% Checks if all orderings of actions in one parallel step are valid.
% The plan is read from the standard input.
main =>
    verify_plan().

% Verifies the plan.
% Expects one number argument: if the argument is equal to 0, verifies only the sequential plan.
% Otherwise, checks if all orderings of actions in one parallel step are valid.
% The plan is read from the standard input.
main([FullS]) =>
    Full = to_int(FullS),
    if Full = 0 then
        verify_plan_relaxed()
    else
        verify_plan()
    end.