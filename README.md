# README #

The program sas_to_picat.pi translates the output of the Fast Downward translator [1] in the format SAS+ to Picat predicates. 

The program picat_to_tcpp.pi translates this description to TCPP [2], picat_to_rres.pi to the R²∃-step encoding [3] and picat_to_reinf.pi to the Reinforced Encoding [4]. 

The programs tcpp_sat.pi, rres_sat.pi and reinf_sat.pi attempt to solve the problem in the corresponding encoding (TCPP, R²∃-step encoding or Reinforced Encoding) with a given makespan and decode the plan.

The file run.sh contains a program for Cygwin which shows how the Picat programs can be used to solve a planning problem.

[1] Fast Downward contributors. Fast Downward 19.06, 2019. Computer Software. Available from http://www.fast-downward.org/.  
[2] Nina Ghanbari Ghooshchi, Majid Namazi, MA Hakim Newton, and Abdul Sattar. Encoding domain transitions for constraint-based planning. Journal of Artificial Intelligence Research, 58:905–966, 2017.  
[3] Tomáš Balyo. Relaxing the relaxed exist-step parallel planning semantics. In 2013 IEEE 25th International Conference on Tools with Artificial Intelligence,
pages 865–871. IEEE, 2013.  
[4] Tomáš Balyo, Roman Barták, and Otakar Trunda. Reinforced encoding for planning as SAT. Acta Polytechnica CTU Proceedings, 2(2):1–7, 2015.